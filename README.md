# Table Creator Project for Web Development II
Group 4

## DESCRIPTION
- This HTML file recieves input from a form (used to create a project) and displays the input in a table. The projects can be stored locally into and loaded from your computer.
- As where it stands now, there's not really a mechanism to avoid any potential issues with memory (since event listeners for the buttons are created continuously, the filtering for editing rows might cause issues as well). This could be fixed in the future via thorough debugging and organizing the row edit mechanic a bit better.
- Javascript, HTML and CSS is used for the project. No external APIs or anything of the sort. Some icons are borrowed from font awesome  

## INSTALLATION
- No installation necessary. If the project is on an existing website, then go to the url on where the file is stored.
- To open it locally, simply type the file path into where you type the URLs in your browser.
(EX: file:///home/nic/webdev-project1/HTML/MainPage.html)

## GIT LAB URL
https://gitlab.com/HowYaDoin/webdev-project1

## CREDITS
- Nicoleta Sarghi & Kelsey Costa for the HTML, CSS and Javascript,
- Section 4 of the Web Development course in Computer Science and Technology program, Dawson College
- Font Awesome for providing all the necessary icons (https://fontawesome.com/)

## LICENSE
MIT License

Copyright (c) 2021 Nicoleta Sarghi, Kelsey Costa

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## CONTRIBUTOR NOTES
If you're wondering why there's a contributor called *Arsenic Apples* on the commit history, that's because the email associated with it is the one I (Nicoleta) use for my github account. I had changed PCs during that time and forgot to set ``git config --local``. You can email the address and I can indeed confirm it is me. I apologize for any confusion. 

## PROGRAM LAYOUT
### FORM HANDLING
- Store all field types in an array. Each field type is an object that indicates the key for project object, the regex filter,the feedback message and the default value. The array serves to determine the order in which the columns are added
- DOM loaded -> Get vairables, Load form and button events
- Form change -> Use Regex expressions stored in the field type object, validate the field based on whether the regex finds a match. Show feedback for the form afterwards (change icon + disable/enable invalid box). Finally, enable/disable the add button.
- Form submission -> Adds a row to the column. Also adds to the project array

### LOCAL DATA BUTTONS
- Write local: Clears the local storage and appends all projects in the projects array to local storage (see function below). 
- Append local: Appends projects to local storage. Does not clear the projects array  
- Clear local: Clears the local storage, removes all rows in local storage. If applicable, removes the rows in the projects array and its row.
- Load local: Loads all projects into the projects array and into the table IF it's not already there.

### PROJECT ROW
- Edit: Changes every column in the row to an input of type text, change image to save icon
- Row Editor: Check column by column to see if each input is valid, if valid, change save icon opacity
- Delete: Asks the user if they want to delete the project. If yes, delete the row, the project data and the entry in localstorage

### QUERY
- Use array.filter to search through the id, owner and title (regex to check if it matches)
- Set all rows to be temporarily invisible, then set the filtered rows to visible.

### SORTING
- Click on a thead cell to sort the rows in alphabetical order
- If it's clicked on again, sort in reverse-alphabetical order
- The thead cell determines how the array is sorted  

