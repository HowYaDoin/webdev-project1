/* MAIN HTML FORM VALIDATION */
/**
 * @author Nicoleta Sarghi
 * @description Displays the feedback text of the input element based on whether its successful or not
 * @param  {string} id The id of the input element 
 * @param  {string} feedbackText Feedback message for an invalid element
 * @param  {boolean} success If the filtering is sucessful
 */
 function insertFeedbackText(id, feedbackText, success){
    let parent = document.querySelector(`#${id}`).parentNode
    let textElement = createOrGetClass("section","box-valid",parent)//getFeedbackElement(id,"box-valid","section") 
    textElement.innerHTML = feedbackText
    textElement.hidden = success
}

/**
 * @author Nicoleta Sarghi
 * @description Displays a feedback icon based on whether or not the input field is valid 
 * @param  {string} id The id of the input element 
 * @param  {boolean} success If the filtering is sucessful
 */
function insertFeedbackIcon(id, success){
    let parent = document.querySelector(`#${id}`).parentNode
    let iconElement = createOrGetClass("img","icon-valid",parent)
    let icon = success ? "../IMAGES/check-circle-regular.svg" : "../IMAGES/times-circle-regular.svg" 
    iconElement.src = icon
}

/**
 * @author Nicoleta Sarghi
 * @description Displays a feedback message/icon for the input (for the form only).
 * @param  {string} elementId The id of the input element 
 * @param  {string} feedbackText Feedback message for an invalid element
 * @param  {boolean} success If the filtering is sucessful
 */
 function setElementFeedback(elementId,feedbackText,success){
    insertFeedbackIcon(elementId, success)
    insertFeedbackText(elementId, feedbackText, success)
}

/** 
 * @author Nicoleta Sarghi
 * @description Returns true when all of the values in the validationFlags object are true
 * @param  {Object} validationFlags The validation flags object
 * @returns {boolean} True if all of the values in the flags are true
 */
function formIsValid(validationFlags){
    for(key in validationFlags){
        if(!validationFlags[key])
            return false;
    } 
    return true;
}

/**
 * @author Kelsey Costa
 * @description Enables/disables the add button in the form 
 */
function setFormEnabled(){
    let enabled = formIsValid(validFormElements) // check if everything is valid
    enableDisableButton("#add-button",enabled) // if its valid, enable
}

/**
 * @author Nicoleta Sarghi
 * @description Set all flags and buttons for the form when it is being reset
 * @param {Object} inputContainer Object containing input elements sorted by their formData key equivalent
 */
function resetForm(inputs){
    for(key in inputs){
        // get element and data
        let element = inputs[key]
        let formValidationData = getFormValidationData(key) // get the data
        let defaultValue = formValidationData.default != null ? formValidationData.default : ""
        // reset element manually
        element.setAttribute("value",defaultValue) // set the value DISPLAY
        element.value = defaultValue // set the value itself
        element.dispatchEvent(new Event('input')) // call the event because setAttribute does not trigger input
    }
}
