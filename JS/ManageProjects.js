/**
 * @author Nicoleta Sarghi
 * @description creates a new object that will be used in other functions
 * @param {array} inputs the values that the user entered into the form
 * @returns {ProjectObject} the created projectobject
 */
function createProjectObject(inputs){
    // an object where all the object ids are the input's id, and the values are the input's value
    let projectObject = {}
    for(formData of projectFormData){
        if(inputs[formData.key]==null){
            throw "Invalid key: "+formData.key
        } 
        projectObject[formData.key] = inputs[formData.key].value
    }
    return projectObject
}
/**
 * @author Kelsey Costa
 * @description adds columns to the newly added rows. Adds buttons to the end of the row
 * @param {node} row the parent row
 * @param {object} projectObject object that holds all the values needed to fill the columns
 */
function addColumnsToRow(row,projectObject){
    // data columns
    let projectValues =Object.values(projectObject)
    let elements = createTagElementsFromArray("td",row,projectValues);
    elements.forEach((element,i)=>{
        element.innerHTML = projectValues[i]
    })
    // button columns
    addColumnButton(row,"edit-button","../IMAGES/edit-solid.svg",()=>editButton(row))
    addColumnButton(row,"delete-button","../IMAGES/trash-alt-solid.svg",()=>promptDeleteProject(row))
}
/**
 * @author Nicoleta Sarghi
 * @description adds a button to the end of the column
 * @param {Node} row the parent row 
 * @param {string} className the class name of the button
 * @param {string} src the image link for the button
 * @param {Function} buttonFunction the function that the button must call when clicked
 */
function addColumnButton(row,className,src,buttonFunction){
    let column = createTagElement("td",row) // create the column and the button itslef
    column.className = "button-column"
    let button = createTagElement("input",column)
    button.setAttribute("type","image") // then set the src + class appripriately 
    button.className = className
    button.src = src
    button.addEventListener("click",buttonFunction) // event listener for clicking
}

/**
 * @author Kelsey Costa
 * @description takes in an object and loops through it adding its values to the table row
 * @param {object} projectObject the validated object that holds all the information to be added to the table
 */
function addProjectRow(projectObject){
    let row = createTagElement("tr",table)
    row.id = "project-row-"+projectObject.id
    addColumnsToRow(row,projectObject)
}

/**
 * @author Kelsey Costa
 * @description redraws the rows according to the projects table 
 */
function updateProjectsTable(){
    createTableFromArrayObjects(projects) 
}

/**
 * @author Nicoleta Sarghi
 * @description Adds a new project row and checks if it already exists
 * @param {object} inputs the values of the form
 */
function addProject(inputs){
    if (projectIdExists(inputs.id.value)) {
        throw `Project with id "${inputs.id.value}" already exists.`
    }
    // Every time a form is sent, a new row is added to the table
    let projectObject = createProjectObject(inputs); // create the object
    // add a row to the projects table and add to the array
    projects.push(projectObject)  
    setStatusBar(`Added new project ${projectObject.id}`)  
    updateProjectsTable()
}

/* TABLE ROW FUNCTIONS */
/**
 * @author Nicoleta Sarghi
 * @description Promps the user to delete a project. Handles the exception if not possible
 * @param {object} row the object that is going to be deleted from the table
 */
function promptDeleteProject(row){
    let id = getIdFromRow(row)
    let answer = confirm(`Please confirm that you want to delete project ${id}`)
    try{
        if(answer) {
            deleteProject(id)
        }
        setStatusBar(`Deleted project ${id}`)
    }
    catch(exception){
        console.warn(exception)
        outputErrorMessage("Could not delete project: "+exception,"#table-manager",true)
    }
}
// 
/**
 * @author Nicoleta Sarghi
 * @description Deletes a project and its row using its id
 * @param {string} id the id of the project row
 */
function deleteProject(id){
    let index = projects.findIndex((project, i)=>project.id==id)
    let inProjects = index != -1
    // if you cant find the project anywhere, something is wrong
    if(!inProjects){
        throw "Could not find project of id "+id
    }
    // remove from screen
    projects.splice(index,1)
    updateProjectsTable()
    toggleSaveButtons()
}

/**
 * @author Kelsey Costa
 * @description Get the current row editing button. The classname gets changed ( in order to tell whether or not its being edited)
 * @param {string} row the row that the edit button was clicked on
 * @returns {[Element,boolean]}
 */
function getRowEditButton(row){
    let button = row.querySelector(".edit-button")
    let isEditing = false
    if(button==null){
      button = row.querySelector(".save-edits-button")
      isEditing = true
    }
    return [button, isEditing]
}

 /**
  * @author Kelsey Costa
  * @description Change the button's icon and className
  * @param {Node} button 
  * @param {string} src 
  * @param {string} className 
  */
function switchButton(button,src,className) {
    button.src = src
    button.className = className
}

/**
 * @author Kelsey Costa
 * @description Takes the input values and writes them to the new table
 * @param {string} id repressents the rows project id
 * @param {Node} row the row that is being edited
 */
function saveEditChanges(id,row){
    // get the project object, try writing
    let filter = new RegExp(`${row.id}-`)
    let inputList = row.querySelectorAll("input")
    let projectObject = createProjectObject(getInputList(inputList,filter))

    // throw an exception if youre trying to set a project an exisitng id
    if (projectIdExists(projectObject.id)&&projectObject.id!=id) {
        throw `Project with id "${projectObject.id}" already exists.`
    }
    overwriteProject(id,projectObject)
    // re-sort
    if(lastKeyClicked){ // only if its been sorted before
        sortColumn(lastKeyClicked,lastAlphabetical)
    }
    updateProjectsTable()
}
/**
 * @author Nicoleta Sarghi
 * @description
 * @param {string} oldProjectId 
 * @param {object} projectObject 
 */
function overwriteProject(oldProjectId,projectObject){
    // try to get the index in the projects table
    let index = projects.findIndex((v)=>v.id==oldProjectId)
    let isInProject = index != -1
    // throw an exception if the project cannot be overwritten
    if(!isInProject){
        throw `Could not find project, OLD: ${oldProjectId}, NEW: ${projectObject.id}` 
    }
    // set the index
    projects[index] = projectObject
    setStatusBar(`Saved changes to project ${projectObject.id}`)
    toggleSaveButtons()
} 
/**
 * @author Kelsey Costa
 * @description handles the edit function, it validates, and turns boxes into editable inputs
 * @param {String} row the row that the button was clicked on
 * @param {Node} button the button that was clicked on
 */
function editTableRow(row,button){
    let rowColumns = row.querySelectorAll("td");
    let rowValidationFlags = {}

    let idPrecursor = `${row.id}-` // idPrecursor used to add before the input ids (mainly to differentiate them)
    rowColumns.forEach((td,index)=>{
        if(index <= 7){ // clear the innerHTML and add in input boxes
            let originalData = td.innerHTML;
            td.innerHTML = "";
            let input = createTagElement("input",td);
            input.className = "edit-row-box";
            input.value = originalData;
            // set the input ids to use later for filtering
            let formFilterData = projectFormData[index]
            let newId = idPrecursor+formFilterData.key
            input.id = newId
        }
    })

    let inputs = getInputList(row.querySelectorAll(`input:not([type="image"])`),new RegExp(idPrecursor))
    let editButtonEnabled = function(){ // Toggle between disabled/enabled when the inputs are filtered
        let enabled = formIsValid(rowValidationFlags) // check if everything is valid
        button.disabled = !enabled
    }
    // minimalistic feedback message function for when the inputs are edited
    let invalidateEditField = function(elementId,feedbackText,success){
        let field = row.querySelector("#"+elementId)
        let msg = success ? "" : feedbackText
        field.setCustomValidity(msg)
    }
    // init regex for the input elements
    initRegex(inputs,rowValidationFlags,editButtonEnabled,invalidateEditField)
    editButtonEnabled()
}

/**
 * @author Kelsey Costa
 * @description calls the appropriate function when the edit button is pressed
 */
function editButton(row){
    let id = getIdFromRow(row)
    let [button,isEditing] = getRowEditButton(row)
    try{
        if (isEditing){
            saveEditChanges(id,row)
            switchButton(button,"../IMAGES/edit-solid.svg","edit-button")
        } 
        else {
            editTableRow(row,button)
            switchButton(button,"../IMAGES/floppy-disk-solid.svg","save-edits-button")
        }
    }
    catch(exception){
        console.warn(exception)
        outputErrorMessage("Could not edit project: "+exception,"#table-manager",true)
    }
}

/* LOCAL DATA STORAGE */
/**
 * @author Nicoleta Sarghi
 * @description helper method to read everything in the local storage and returns an array
 * @returns {[string]} A list of all the keys used in localStorage
 */
function getAllStorageData(){
    let keys = []
    for(i=0;i<localStorage.length;i++){
        let key = localStorage.key(i)
        keys.push(key)
    }
    return keys
}

/**
 * @author Nicoleta Sarghi
 * @description Overwrite the local storage and add everything in the projects array to it
 */
function saveAllProjects2Storage(){
    localStorage.clear()
    appendAllProjects2Storage()
    setStatusBar(`Saved all projects to local storage`)
}
/**
 * @author Nicoleta Sarghi
 * @description loads all saved projects in local storage to the table
 */
function readAllProjectsFromStorage(){
    let keys = getAllStorageData()
    // get all sessionstorage data and create rows
    for(key of keys){ // insert the data thats not in the projects array
        let notInProjects = projects.find((v,i)=>v.id==key)==null
        let projectObject = JSON.parse(localStorage.getItem(key))
        if(notInProjects){
            projects.push(projectObject)
        }        
    }
    updateProjectsTable()
    toggleSaveButtons()
    setStatusBar(`Loaded ${keys.length} projects from local storage`)
}


/**
 * @author Nicoleta Sarghi
 * @description Appends the projects table to localStorage
 */
function appendAllProjects2Storage(){
    let addedCount = 0
    projects.forEach(function(project){
        let key = project.id
        let projectString = JSON.stringify(project)
        if(!localStorage.getItem(key)){
            addedCount++
        }
        else{
            let isEquivalent = localStorage.getItem(key)==JSON.stringify(project)
            if(!isEquivalent){
                addedCount++
            }             
        } 
        localStorage.setItem(key,projectString)
    })
    setStatusBar(`Saved or overwrote ${addedCount} projects to local storage`)
    toggleSaveButtons()
}
/**
 * @author Nicoleta Sarghi
 * @description Clears all of the projects from the storage locally and removes all rows
 */
function clearAllProjectsFromStorage(){
    let answer = confirm(`Please confirm that you want to clear all your projects from storage`)
    try{ // try catch for clearing
        if(answer){ // if user confirms, try to clear
            localStorage.clear()
            setStatusBar(`Cleared local storage`)
        } 
        toggleSaveButtons()
    }
    catch(exception){
        console.warn(exception)
        outputErrorMessage("Could not clear projects: "+exception,"#table-manager",true)
    }
}
/**
 * @author Nicoleta Sarghi
 * @description checks if a project id already exists
 * @param {string} id represents a project id to be validated
 * @returns {boolean} True if there's a matching project id 
 */
function projectIdExists(id){
    let arrayFind = projects.findIndex((v)=>v.id==id)
    return arrayFind!=-1
}

/* QUERY FILTER */
/**
 * @author Kelsey Costa
 * @description filters rows matching an input and hides rows who doesnt match
 * @param {string} input represents the value in the search bar
 */
function filterProjects(input){
    let rQuery = new RegExp("^" + input);
    let filteredProjects = projects.filter((elem) => { // filter the projects array with an or statement 
        return rQuery.test(elem.id) || rQuery.test(elem.owner) || rQuery.test(elem.title)
        || rQuery.test(elem.category) || rQuery.test(elem.status)
        || rQuery.test(elem.rate) || rQuery.test(elem.hours)
        || rQuery.test(elem.description) ;
    });
    // generate the table using the sub array  
    createTableFromArrayObjects(filteredProjects)
    setStatusBar(`Found ${filteredProjects.length} results for your query`)
}
/* SORT FILTER */   
/**
 * @author Kelsey Costa
 * @description a method which sorts the button row alphabetically 
 * @param {string} key unique key associated with the projects data object
 * @param {boolean} alphabetical the last time the button was clicked
 */
function sortColumn(key,alphabetical){
    let getValue = (value)=>{ // Get the value numerically if its a numerical value
        if(key=="hours" || key=="rate"){
            return Number.parseInt(value)
        }
        return value
    }
    lastKeyClicked = key
    // Sort the projects based on if its alphabetical or not, then draw table
    projects.sort((a,b)=>{
        let aValue = getValue(a[key])
        let bValue = getValue(b[key])
        if(aValue < bValue){
            return alphabetical? -1 : 1;
        } else if (aValue > bValue){
            return alphabetical? 1 : -1;
        } else{
            return 0;
        } 
    });
    updateProjectsTable()
}

