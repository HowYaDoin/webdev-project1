/**
 * @author Kelsey Costa
 * @description Gets a button through queryselector and enables/disables it
 * @param  {string} query Query to select the button (used by query selector)
 * @param  {boolean} parent Parent Node
 */
function enableDisableButton(query,enabled){
    let button = document.querySelector(query)
    button.disabled = !enabled
}
/**
 * @author Kelsey Costa
 * @description Creates an element based on a tag name and parents it  
 * @param  {string} tag Tag name
 * @param  {Node} parent Parent Node
 * @returns {Element} Created element
 */
function createTagElement(tag, parent){
    let element = document.createElement(tag)
    parent.appendChild(element)
    return element
}
/**
 * @author Kelsey Costa
 * @description Acquires an element based on a class name. If it doesnt exist, create it.
 * @param  {string} tag Tag name
 * @param  {Node} parent Parent Node
 * @returns {Element} Created or acquired element
 */
function createOrGetClass(tag,className,parent){
    let classElement = parent.querySelector("."+className)
    if(classElement==null){
        classElement = createTagElement(tag,parent)
        classElement.setAttribute("class",className)
    }
    return classElement
}
/**
 * @author Nicoleta Sarghi
 * @description Calls createTagElement for each entry in an array
 * @param  {string} tag Tag name
 * @param  {Node} parent Parent Node
 * @param  {Array} array Array being iterated through
 * @param  {(entry,i,element)} callback callback function, optional
 * @returns {[Element]} The list of elements created
 */
function createTagElementsFromArray(tag,parent,array,callback){
    let elements = []
    array.forEach((entry,i)=>{
        // create tag
        let element = createTagElement(tag,parent)
        if (callback){
            callback(entry,i,element)
        }
        // add element to array
        elements.push(element)
    })
    return elements
}
/* VALIDATION */
/**
 * @author Nicoleta Sarghi
 * @description Adds an error message to a parent with a given message (used for try/catch blocks)
 * @param  {string} message Message to display for the error
 * @param  {string} parentTag Selector to get the parent nodesi pre
 * @param  {boolean?} prepend Optional boolean that determines whether or the box is prepended to its parent   
 */
function outputErrorMessage(message,parentTag,prepend){
    // get the parent and error box, set the message
    let parent = document.querySelector(parentTag)
    let errorBox = createOrGetClass("section","error-message",parent)
    errorBox.innerHTML = message
    if(prepend){ // prepend if necessary
        parent.prepend(errorBox)
    }
    // function to remove the box when it is being clicked outside of it
    let removeChildFunction 
    removeChildFunction = (event)=>{
        if(event.target!=errorBox){
            parent.removeChild(errorBox)
            document.removeEventListener("mouseup",removeChildFunction)
        }
    }
    document.addEventListener("mouseup",removeChildFunction)
}
/**
 * @author Nicoleta Sarghi
 * @description Sets a required input element's label to bold
 * @param  {Element} input The input element 
 */
function setElemRequired(input){
    let inputLabel = document.querySelector(`label[for="${input.id}"]`)
    if(inputLabel != null) {
        inputLabel.setAttribute("class","required-item")
    }
}
/**
 * @author Nicoleta Sarghi
 * @description Validation function that sets a flag in an object to true/false based on the result of regex.test
 * @param  {string} key The key in the validationFlags being modified 
 * @param  {Element} input The input element being filtered
 * @param  {RegExp} regex The regex expression used to filter the input
 * @param  {Object} validationFlags The flag object being modified
 * @returns {boolean} True if the input is valid
 */
function validateElement(key, input, regex, validationFlags){
    let isValid = regex.test(input.value)
    validationFlags[key] = isValid
    return isValid
}
/**
 * @author Nicoleta Sarghi
 * @description Validates an input and calls methods whenever an input is changed
 * @param  {string} key The key in the validationFlags being modified 
 * @param  {Element} input The input element being filtered
 * @param  {Object} formValidationData the form validation data containing the key, regex and feedback message
 * @param  {Object} validationFlags The flag object being modified
 * @param  {()} formEnabledFunction The function called to modify the form after the element was validated
 * @param {(elementId,feedbackText,success)} feedbackFunction The feedback function being called
 */
function setElementValidation(key,input,formValidationData,validationFlags,formEnabledFunction,feedbackFunction){
    let isValid = validateElement(key,input,formValidationData.pattern,validationFlags)  
    formEnabledFunction()
    feedbackFunction(input.id,formValidationData.invalidMsg,isValid)
}
/**
 * @author Nicoleta Sarghi
 * @description Inits regex validation for an array of inputs whenever their value is changed
 * @param  {NodeList} inputs The NodeList array consisting entirely of input elements
 * @param  {Object} validationFlags The flag object used to keep track of which keys are valid/invalid
 * @param  {()} formEnabledFunction The function called to modify the form after the element was validated
 * @param {(elementId,feedbackText,success)} feedbackFunction The feedback function being called
 */
function initRegex(inputs,validationFlags,formEnabledFunction,feedbackFunction){ 
    for(key in inputs){
        let element = inputs[key]
        let formValidationData = getFormValidationData(key)
        let pattern = formValidationData.pattern    
        // if theres a pattern (thus its required, then listen on input for changes)
        if(pattern){
            setElemRequired(element)
            element.addEventListener("input",()=>setElementValidation(key,element,formValidationData,validationFlags,formEnabledFunction,feedbackFunction),true)
            setElementValidation(key,element,formValidationData,validationFlags,formEnabledFunction,feedbackFunction)
        } 
    }
}

/**
 * @author Nicoleta Sarghi
 * @description Get the validation data for the form via the input id
 * @param  {string} key The unique key associated with each form data object
 * @returns {FormData} Form validation data associated with the key
 */
function getFormValidationData(key){
    // find the input based on the id using "find"
    let formValidationData = projectFormData.find((data,i)=>data.key==key)
    if (!formValidationData){
        throw `Could not find project field for ${key} (${element.id})`
    }
    return formValidationData
} 
/**
 * @author Nicoleta Sarghi
 * @description Returns an object containing inputs sorted by the input's filtered id (key). The id is filtered with a regexp
 * @param  {NodeList} inputList The inputs being processed
 * @param {RegExp} filterId The RegExp used to filter out ('delete') the id of each input element
 * @returns {Object} The input elements object sorted by key 
 */
function getInputList(inputList,filterId){
    let mappedInputKeys = Array.from(inputList).map((input)=>input.id.replace(filterId,""))
    let inputs = {}
    mappedInputKeys.forEach((key,i)=>{
        inputs[key] = inputList[i]
    })
    return inputs
} 
/**
 * @author Nicoleta Sarghi
 * @description Sets the status bar element's text to the desired message
 * @param  {string} message The new message to be set 
 */
function setStatusBar(message){
    let statusBar = document.querySelector("#status-bar")
    statusBar.innerHTML = message
}

/**
 * @author Nicoleta Sarghi
 * @description Returns the project id with a given project row
 * @param {Element} row The row representing a project 
 * @returns {string} Project id
 */
function getIdFromRow(row){
    return row.id.replace(/project-row-/,"")
}

/**
 * @author Nicoleta Sarghi
 * @description Draws all of the rows each representing a project, with the specified project array
 * @param {[ProjectObject]} projectArray The project objects to be drawn to the screen
 */
function createTableFromArrayObjects(projectArray){
    // remove all the old rows
    let oldRows = document.querySelectorAll("#main-table>tr")
    oldRows.forEach((row)=>{
        table.removeChild(row)
    })
    // add in new rows
    projectArray.forEach((project)=>{
        addProjectRow(project)
    })
}
