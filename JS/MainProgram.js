/* PROJECT GLOBAL VARIABLES */
/**
 * @typedef ProjectObject
 * @type {Object}
 * @property {string} id Unique project id
 * @property {string} owner Project owner
 * @property {string} title Project title
 * @property {string} category The type of project category (research, etc.) 
 * @property {string} status The status of the project
 * @property {string} hours How long the project lasts in hours
 * @property {string} rate Hourly rate ($) dedicated to advancing the project 
 * @property {string} description Brief project description
 */

/**
 * @type {[ProjectObject]}
 * @description Array storing the projects on screen 
 */
let projects = []

/**
 * @description object storing the input elements in the main form, sorted by key
 */
let formInputs = {}

/**
 * @description Object storing the flags for valid/invalid elements in the form. Sorted by key.
 */
let validFormElements = {}

let form // form element
let table // table element 

/**
 * @description the last sorting form key clicked (project sorting) 
 */
let lastKeyClicked

/**
 * @description whether the last sorting Alphabetical or not (project sorting)
 */
let lastAlphabetical = true

// Determines the order in which the values are added in  
// Stores the Project Object id name, the regex to be used and the invalidation message
/**
 * @typedef FormData
 * @type {Object}
 * @property {string} key The unique key used to sort several objects. It represents a column in a row or the input's identifier
 * @property {RegExp} pattern The regex pattern that validates the input associated to the key
 * @property {string} invalidMsg The error message being displayed to the end user if the input is flagged as invalid
 * @property {?string} default The default value for the input.
 */

/**
 * @type {[FormData]}
 * @description Represents the key identifiers and fiter-related data for the inputs.  The form data values are sorted by how the columns are inserted into the row. 
 */
let projectFormData = [
    { key:"id",         pattern: /^\w+$/,                   invalidMsg: "Alphanumeric characters only" },
    { key:"owner",      pattern: /^[A-Za-z\s]+$/,           invalidMsg: "Alphanumeric chars + whitespace only" },
    { key:"title",      pattern: /\w+/,                     invalidMsg: "At least 1 alphanumeric char" },
    { key:"category",   pattern: /(theoretical|practical|fundamental research|empirical)/,invalidMsg: "Please select a category",default:"nullVal"},
    { key:"status",     pattern: /(completed|ongoing|planned)/,invalidMsg: "Please select a status",                           default:"nullVal"}, 
    { key:"hours",      pattern: /^([1-9]|[1-9][0-9]*)$/,   invalidMsg: "Please pick a number (min 1)",             default:"20"},
    { key:"rate",       pattern: /^([1-9]|[1-9][0-9]|100)$/,invalidMsg: "Please pick a number (min 1, max 100)",    default:"10"},
    { key:"description",pattern: /\w+/,                     invalidMsg: "At least 1 alphanumeric char",} 
]

/* ADD PROJECT (FORM SUBMIT) */ 
/**
 * @author Kelsey Costa
 * @description Form submission callback function, adds a row to the table + handles input
 * @param  {Event} event The event being fired 
 */
function submitForm(event){
    event.preventDefault()
    try {
        if(!formIsValid())
            throw "Form is not valid." 
        addProject(formInputs) // add a project
        toggleSaveButtons() // save buttons have to be updated (because of append)
    }
    catch(exception) {
        console.warn(exception)
        outputErrorMessage("Could not create project: "+exception,"#input-widgets")
    }
}
/* HTML VARIABLE INIT */ 
/**
 * @author Kelsey Costa
 * @description Initialize the form input elems + validation for the page  
 */
 function initInput(){    
    let inputList = document.querySelectorAll("#input-widgets input,select,textarea")
    formInputs = getInputList(inputList,/project-/) // get the input list from the project inputs 
    // init regex
    initRegex(formInputs,validFormElements,setFormEnabled,setElementFeedback)
    setFormEnabled()
}
/**
 * @author Nicoleta Sarghi
 * @description Toggles the write/append/clear/read buttons to enabled/disabled based on the status of the localstorage or the projects array
 */
function toggleSaveButtons(){
    // if the projects array is empty, disable the write button
    let notEmpty = projects.length > 0
    enableDisableButton("#write-local",notEmpty)
    // if there are no NEW projects to append to local storage, disable the append button 
    let storageToFill = projects.reduce((previous,current,index)=>{
        let storageObject = localStorage.getItem(current.id)
        let add = 1
        if (storageObject!=null){
            add = storageObject==JSON.stringify(current)? 0:1
        }
        return previous+add
    },0)
    enableDisableButton("#append-local",storageToFill > 0)
    // if there is nothing to clear, then disable the clear and load button    
    let storageNotEmpty = localStorage.length>0
    enableDisableButton("#clear-local",storageNotEmpty)
    enableDisableButton("#load-local",storageNotEmpty)
}
/**
 * @author Nicoleta Sarghi
 * @description Initializes all of the event listeners for the buttons in the page
 */
function setButtons(){
    // Buttons for manipulating the local storage
    let writeLocal = document.querySelector("#write-local")
    let appendLocal = document.querySelector("#append-local")
    let clearLocal = document.querySelector("#clear-local")
    let loadLocal = document.querySelector("#load-local")
    // event listeners
    writeLocal.addEventListener("click",saveAllProjects2Storage)
    appendLocal.addEventListener("click",appendAllProjects2Storage)
    clearLocal.addEventListener("click",clearAllProjectsFromStorage)
    loadLocal.addEventListener("click",readAllProjectsFromStorage)
    toggleSaveButtons()
}
// Init form and events
/**
 * @author Nicoleta Sarghi
 * @description Acquire the form + table elements and initialize the event listeners for it
 */
function setForm(){
    // populate inputs
    initInput()
    // get the form object
    form = document.querySelector("form")
    table = document.querySelector('#main-table')
    form.addEventListener("reset",()=>resetForm(formInputs))
    form.addEventListener("submit",submitForm)
}
/**
 * @author Kelsey Costa
 * @description Initialize the event listener for the search function
 */
function setQueryFilter(){
    query = document.querySelector("#search-query");
    query.addEventListener('input',()=>filterProjects(query.value));
}
//
/**
 * @author Kelsey Costa
 * @description Initialize all the event listeners for the theads so that the rows may be sorted
 */
function setSortButtons(){
    let buttonArr = document.querySelectorAll(".column-button");
    buttonArr.forEach((element,index)=>{ /// loop through column buttons and listen for a click
        let key = projectFormData[index].key
        element.addEventListener('click',()=>{
            changedKey = key != lastKeyClicked
            lastAlphabetical = changedKey || lastAlphabetical==false // set the alphabetical order
            sortColumn(key,lastAlphabetical)
        });
    });
}
/**
 * @author Nicoleta Sarghi
 * @description Initializes the main program when the DOM is fully loaded
 */
function main(){
    // Init the form and buttons
    setForm()
    setButtons()
    setQueryFilter()
    setSortButtons()
    // update status when finished loading
    setStatusBar(`There are ${localStorage.length} projects in local storage`)
}
window.addEventListener("DOMContentLoaded",main)